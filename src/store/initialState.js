import nopic from "./nopic.png";

const initialState = {
  auth: {
    token: "",
    err: "",
  },

  app: {
    chats: [],
    isSearching: false,
    searchInput: "",
  },

  profile: {
    picUrl: nopic,
    autogreeting: "Hi!",
    defaultMessages: ["hello", "how are you"],
  },
};

export default initialState;
