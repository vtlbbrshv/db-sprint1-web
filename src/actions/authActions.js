import { actions } from "../lib/constants";

export const deleteToken = () => ({
  type: actions.DELETE_TOKEN,
});

export const exit = firebase => ({
  type: actions.EXIT,
  firebase: firebase,
});

export const reqFailed = (type, err) => ({
  type: type,
  err: err,
});

export const reqSuccess = (type, token) => ({
  type: type,
  token: token,
});

export const validateId = firebase => ({
  type: actions.VALIDATE_ID,
  firebase: firebase,
});

export const formRequest = (type, firebase, email, password) => ({
  type: type,
  firebase: firebase,
  email: email,
  password: password,
});
