import { all } from "redux-saga/effects";
import { watchAuthSaga } from "./authSaga";
import { watchAppSaga } from "./appSaga";
import { watchProfileSaga } from "./profileSaga";

export default function* rootSaga() {
  yield all([watchAuthSaga(), watchAppSaga(), watchProfileSaga()]);
}
