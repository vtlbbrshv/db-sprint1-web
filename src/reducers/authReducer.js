import { actions } from "../lib/constants";

const auth = (state = {}, action = { type: null }) => {
  switch (action.type) {
    case actions.REGISTRATION_SUCCESS: {
      alert("Регистрация прошла успешно");
      return state;
    }

    case actions.LOGIN_SUCCESS: {
      return { ...state, token: action.token, err: "" };
    }

    case actions.DELETE_TOKEN: {
      return { ...state, token: "" };
    }

    case actions.REGISTRATION_FAILURE: {
      return { ...state, err: action.err };
    }

    case actions.LOGIN_FAILURE: {
      return { ...state, err: action.err };
    }

    default:
      return state;
  }
};

export default auth;
