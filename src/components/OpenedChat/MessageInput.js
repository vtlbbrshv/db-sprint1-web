import React, { Component } from "react";
import { connect } from "react-redux";
import { getFirebase } from "react-redux-firebase";
import { sendMessage } from "../../actions/appActions";
import { AiOutlineSend } from "react-icons/ai";
import { emojify } from "react-emojione";
import Picker from "emoji-picker-react";
import { GrEmoji } from "react-icons/gr";
import Autocomplete from "react-autocomplete";

class MessageInput extends Component {
  constructor(props) {
    super(props);
    this.state = { input: "", showEmoji: false };
    this.changeHandler = this.changeHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.onEmojiClick = this.onEmojiClick.bind(this);
  }

  firebase = getFirebase();
  database = this.firebase.database();

  changeHandler = e => {
    e.preventDefault();
    this.setState({ input: e.target.value });
  };

  submitHandler = e => {
    e.preventDefault();
    if (this.state.input) {
      this.props.sendMessage(
        this.database,
        this.props.id,
        this.props.messageNumber,
        this.state.input
      );
    }
    e.target[0].value = "";
    this.setState({ input: "" });
  };

  onEmojiClick = (e, emojiObject) => {
    this.setState({ input: this.state.input + emojiObject.emoji });
  };

  render() {
    return (
      <div className="message-input">
        {this.state.showEmoji ? (
          <Picker onEmojiClick={this.onEmojiClick} />
        ) : (
          ""
        )}
        <button
          onClick={() => this.setState({ showEmoji: !this.state.showEmoji })}
        >
          <GrEmoji />
        </button>
        <form onSubmit={this.submitHandler}>
          <Autocomplete
            getItemValue={item => item.label}
            items={[{ label: "apple" }, { label: "banana" }, { label: "pear" }]}
            renderItem={(item, isHighlighted) => (
              <div
                style={{ background: isHighlighted ? "lightgray" : "white" }}
              >
                {item.label}
              </div>
            )}
            onChange={this.changeHandler}
            value={emojify(this.state.input, { output: "unicode" })}
            //onSubmit={() => (this.value = "")}
            renderInput={props => (
              <input placeholder="сообщение..." {...props}></input>
            )}
          />

          <button type="submit">
            <AiOutlineSend />
          </button>
        </form>
      </div>
    );
  }
}

const connectedMessageInput = connect(null, dispatch => ({
  sendMessage(database, chatId, messageId, message) {
    dispatch(sendMessage(database, chatId, messageId, message));
  },
}))(MessageInput);

export default connectedMessageInput;
