import React from "react";
import Chat from "./Chat";
import InfiniteScroll from "react-infinite-scroll-component";

const Scroller = ({ chats = [], onScrollerEnd = f => f }) => (
  <div className="scroller">
    <InfiniteScroll
      dataLength={chats ? chats.length : 0}
      next={onScrollerEnd}
      hasMore={true}
    >
      {chats
        ? chats.map((chat, i) => <Chat key={i} chat={chat} />)
        : "Загрузка...."}
    </InfiniteScroll>
  </div>
);

export default Scroller;
