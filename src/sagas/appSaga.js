import { call, put, takeLatest } from "redux-saga/effects";
import { actions } from "../lib/constants";
import { addChatsToState, updateChats } from "../actions/appActions";
import { search, sortByStatus } from "../lib/sortFunctions";

//TODO добавлять настройки диалогов в данные юзера на бэкенде
//TODO последний элемент не выводится при поиске

function* getItemsFromServerSaga(action) {
  let chats = [];
  let i = 0;
  console.log(action.limit, action.status);
  while (chats.length <= action.limit) {
    console.log(i);
    // eslint-disable-next-line no-loop-func
    const res = yield call(() =>
      action.database
        .ref("chats/")
        .limitToFirst(action.limit + i)
        .once("value")
        .then(snapshot => snapshot.val())
        .catch(err => console.log(err))
    );

    chats = sortByStatus(res, action.status);
    if (!res[action.limit + i - 1]) break;
    i += action.limit;
  }
  console.log(chats);
  action.searchInput
    ? yield put(addChatsToState(search(chats, action.searchInput)))
    : yield put(addChatsToState(chats));
}

function* changeStatusOnServerSaga(action) {
  yield call(() =>
    action.database.ref("chats/" + action.id).update({
      status: action.newStatus,
    })
  );
  console.log(action.limit, action.id, action.status);
  yield put(updateChats(action.database, action.limit, "", action.status));
}

function* sendMessageSaga(action) {
  yield call(() =>
    action.database.ref("chats/" + action.chatId + "/messages/").update({
      [action.messageId]: {
        content: action.message,
        timestamp: "20200101",
        writtenBy: "operator",
      },
    })
  );

  const res = yield call(() =>
    action.database
      .ref("chats/")
      .limitToFirst(action.chatId + 1)
      .once("value")
      .then(snapshot => snapshot.val())
      .catch(err => console.log(err))
  );
  yield put(addChatsToState(res));
}

export function* watchAppSaga() {
  yield takeLatest(actions.UPDATE_CHATS, getItemsFromServerSaga);
  yield takeLatest(actions.CHANGE_STATUS, changeStatusOnServerSaga);
  yield takeLatest(actions.SEND_MESSAGE, sendMessageSaga);
}
