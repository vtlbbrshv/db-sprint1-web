import React from "react";
import moment from "moment";

const message = ({ message = {} }) => (
  <div>
    {message ? (
      <div className="message">
        <span
          className="message-timestamp"
          style={{ backgroundColor: "rgba(229, 229, 229, 1)" }}
        >
          {moment(message.timestamp, "YYYYMMDD").fromNow()}
          {"\b"}
        </span>
        <span
          className="message-author"
          style={{ backgroundColor: "rgba(229, 229, 229, 1)" }}
        >
          {message.writtenBy}
        </span>
        <div className="message-content">
          {message.content.includes(".jpeg") ? (
            <img
              src={message.content}
              alt="alt prop"
              style={{ maxHeight: "500px", maxWidth: "500px" }}
            />
          ) : (
            <span>{message.content}</span>
          )}
        </div>
      </div>
    ) : (
      ""
    )}
  </div>
);

export default message;
