import React from "react";
import MainPage from "./MainPage/MainPage";
import LoginForm from "./LoginForm";
import { Switch, Route } from "react-router-dom";
import Whoops404 from "./Whoops404";
import { connect } from "react-redux";
import OpenedChat from "./OpenedChat/OpenedChat";
import { routes, status } from "../lib/constants";
import PubNub from "pubnub";
import { PubNubProvider, usePubNub } from "pubnub-react";

const App = ({ store, token }) => (
  <Switch>
    <Route
      path="/"
      component={() => (
        <div className="app">
          <Switch>
            <Route
              path="/"
              component={() =>
                token ? (
                  <div className="chats">
                    <Switch>
                      <Route
                        exact
                        path="/"
                        component={() => <MainPage status={status.ACTIVE} />}
                      />
                      <Route
                        path={routes.NEW_CHATS}
                        component={() => <MainPage status={status.NEW} />}
                      />
                      <Route
                        path={routes.SAVED_CHATS}
                        component={() => <MainPage status={status.SAVED} />}
                      />
                      <Route
                        path={routes.COMPLETED_CHATS}
                        component={() => <MainPage status={status.COMPLETED} />}
                      />
                      <Route path="/:id" component={OpenedChat} />

                      <Route component={Whoops404} />
                    </Switch>
                  </div>
                ) : (
                  <LoginForm store={store} />
                )
              }
            />
          </Switch>
        </div>
      )}
    />
  </Switch>
);
const connectedApp = connect(state => ({
  token: state.auth.token,
}))(App);

export default connectedApp;
