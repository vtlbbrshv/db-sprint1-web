import React from "react";
import { Form, Field } from "react-final-form";
import { connect } from "react-redux";
import { changeUserSettings } from "../../actions/profileActions";
import { getFirebase } from "react-redux-firebase";

const ProfileSettingsForm = ({
  changeSettings = f => f,
  closeModal = f => f,
}) => {
  const firebase = getFirebase();
  const user = firebase.auth().currentUser;

  const onSubmit = values => {
    changeSettings(user, values.email, values.password, values.pic);
    closeModal();
  };

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{ email: user.email, pic: user.photoURL }}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div>
            <label>Email</label>
            <Field name="email" component="input" placeholder="email" />
          </div>
          <div>
            <label>Password</label>
            <Field name="password" component="input" placeholder="password" />
          </div>
          <div>
            <label>Profile pic</label>
            <Field name="pic" component="input" placeholder="url" />
            <img
              src={user.photoURL}
              alt="alt prop"
              style={{ maxHeight: "100px", maxWidth: "100px" }}
            />
          </div>
          <button type="submit">Сохранить</button>
        </form>
      )}
    />
  );
};

const connectedProfileSettingsForm = connect(null, dispatch => ({
  changeSettings(user, email, password, pic) {
    dispatch(changeUserSettings(user, email, password, pic));
  },
}))(ProfileSettingsForm);

export default connectedProfileSettingsForm;
