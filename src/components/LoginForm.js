import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { inputField } from "./Fields";
import { email, required } from "../lib/inputCheck";
import { actions } from "../lib/constants";
import { connect } from "react-redux";
import { getFirebase } from "react-redux-firebase";
import { formRequest } from "../actions/authActions";

export class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
    this.updateState = this.updateState.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
  }

  firebase = getFirebase();

  updateState = e => this.setState({ [e.target.name]: e.target.value });

  handleLogin = e => {
    e.preventDefault();
    this.props.request(
      actions.LOGIN_REQUEST,
      this.firebase,
      this.state.email,
      this.state.password
    );
  };

  handleRegister = e => {
    e.preventDefault();
    this.props.request(
      actions.REGISTRATION_REQUEST,
      this.firebase,
      this.state.email,
      this.state.password
    );
  };

  render() {
    const { err } = this.props;
    const { emailI, password } = this.state;
    // console.log(this.props.store.auth.token);
    return (
      <form className="login-form" onChange={this.updateState}>
        <Field
          name="email"
          component={inputField}
          type="email"
          label="email"
          value={emailI}
          validate={[required, email]}
        />
        <Field
          name="password"
          component={inputField}
          type="password"
          label="password"
          value={password}
          validate={[required]}
        />
        {err ? <p>{err}</p> : ""}
        <button onClick={this.handleLogin}>Войти</button>
        <button onClick={this.handleRegister}>Регистрация</button>
      </form>
    );
  }
}

const form = connect(
  state => ({ err: state.auth.err }),
  dispatch => ({
    request(type, firebase, email, password) {
      dispatch(formRequest(type, firebase, email, password));
    },
  })
)(LoginForm);

export default reduxForm({
  form: "login",
})(form);
