import React from "react";
import Modal from "react-modal";
import { GrUserSettings, GrClose } from "react-icons/gr";
import ProfileSettingsForm from "./ProfileSettingsForm";
import ChatsSettingsForm from "./ChatsSettingsForm/ChatsSettingsForm";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

const UserSettings = () => {
  Modal.setAppElement(document.getElementById("root"));
  const [modalIsOpen, setIsOpen] = React.useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <div className="user-settings">
      <div className="button" onClick={openModal}>
        <GrUserSettings />
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
      >
        <h2>Настройки пользователя</h2>
        <button onClick={closeModal}>
          <GrClose />
        </button>
        <ProfileSettingsForm closeModal={closeModal} />
        <h2>Настройки чата</h2>
        <ChatsSettingsForm closeModal={closeModal} />
      </Modal>
    </div>
  );
};

export default UserSettings;
