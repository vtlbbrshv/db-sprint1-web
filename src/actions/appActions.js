import { actions } from "../lib/constants";

export const addChatsToState = data => ({
  type: actions.ADD_CHATS_TO_STATE,
  data: data,
});

export const updateChats = (database, limit, searchInput, status) => ({
  type: actions.UPDATE_CHATS,
  database: database,
  limit: limit,
  searchInput: searchInput,
  status: status,
});

export const search = input => ({
  type: actions.SEARCH,
  input: input,
});

export const changeStatus = (database, id, newStatus, limit, status) => ({
  type: actions.CHANGE_STATUS,
  database: database,
  id: id,
  newStatus: newStatus,
  limit: limit,
  status: status,
});

export const sendMessage = (database, chatId, messageId, message) => ({
  type: actions.SEND_MESSAGE,
  database: database,
  chatId: chatId,
  messageId: messageId,
  message: message,
});
