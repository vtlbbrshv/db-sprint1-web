import React from "react";

export const inputField = field => (
  <div className="input-row">
    <input {...field.input} placeholder={field.label} />
    {field.meta.touched && field.meta.error && (
      <span className="error">{field.meta.error}</span>
    )}
  </div>
);
