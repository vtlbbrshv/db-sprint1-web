import React, { Component } from "react";
import * as _ from "lodash";
import { constants } from "../../lib/constants";
import { connect } from "react-redux";
import { search } from "../../actions/appActions";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: "",
    };
    this.changeHandler = this.changeHandler.bind(this);
    this.doSearchDebounce = this.doSearchDebounce.bind(this);
  }

  doSearchDebounce = _.debounce(
    () => this.props.doSearch(this.state.input),
    500
  );

  componentDidUpdate() {
    this.doSearchDebounce();
  }

  changeHandler = e => {
    e.preventDefault();
    this.setState({ input: e.target.value });
  };

  render() {
    return (
      <div className="search">
        <input placeholder="поиск..." onChange={this.changeHandler}></input>
      </div>
    );
  }
}

const connectedSearch = connect(null, dispatch => ({
  doSearch(input) {
    dispatch(search(input));
  },
}))(Search);

export default connectedSearch;
