import React from "react";
import { connect } from "react-redux";
import { Field, FieldArray, reduxForm } from "redux-form";
import { inputField } from "../../Fields";
import { setGreeting } from "../../../actions/profileActions";
import DefaultMessagesFields from "./DefaultMessagesFields";

const ChatsSettingsForm = ({
  closeModal = f => f,
  submitGreeting = f => f,
  greeting = "",
  defaultMessages = [],
}) => {
  const handleSubmit = e => {
    closeModal();
    submitGreeting(e.target.autogreeting.value);
  };
  return (
    <form onSubmit={handleSubmit}>
      <h2>Приветствие</h2>
      <Field
        name="autogreeting"
        type="text"
        component={inputField}
        label={greeting}
      />
      <button type="submit">Сохранить</button>
      <h2>Готовые сообщения</h2>
      <FieldArray
        name="defaultMessages"
        fields={defaultMessages}
        component={DefaultMessagesFields}
      />
    </form>
  );
};

const connectedChatsSettingsForm = connect(
  state => ({
    greeting: state.profile.autogreeting,
    defaultMessages: state.profile.defaultMessages,
  }),
  dispatch => ({
    submitGreeting(greeting) {
      dispatch(setGreeting(greeting));
    },
  })
)(ChatsSettingsForm);

export default reduxForm({
  form: "сhatsSettingsForm",
})(connectedChatsSettingsForm);
