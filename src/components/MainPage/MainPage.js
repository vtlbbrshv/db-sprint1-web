import React, { Component } from "react";
import { connect } from "react-redux";
import SideMenu from "./SideMenu";
import Search from "./Search";
import { exit } from "../../actions/authActions";
import { setNewProfilePic } from "../../actions/profileActions";
import { updateChats } from "../../actions/appActions";
import { getFirebase } from "react-redux-firebase";
import { constants } from "../../lib/constants";
import * as _ from "lodash";
import Chats from "../Chats/Chats";
import "../../stylesheets/MainPage.scss";

//TODO первый рефетч производить моментально, после чего увеличить таймер до 30 секунд (не поможет)

class MainPage extends Component {
  firebase = getFirebase();
  database = this.firebase.database();

  doGetChatsFromDatabase = length =>
    this.props.getChatsFromDatabase(
      this.database,
      length
        ? length
        : this.props.chats.length > constants.PAGE_LENGTH
        ? this.props.chats.length
        : constants.PAGE_LENGTH,
      this.props.searchInput,
      this.props.status
    );

  doGetChatsFromDatabaseThrottle = _.throttle(
    () => this.doGetChatsFromDatabase(),
    30000
  );

  componentDidMount() {
    this.firebase.auth().onAuthStateChanged(user => {
      this.props.setUserPic(user.photoURL);
      if (user && user.uid !== this.props.token) {
        this.props.exit(this.firebase);
      }
    });
    this.doGetChatsFromDatabase(constants.PAGE_LENGTH);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.searchInput !== this.props.searchInput) {
      this.doGetChatsFromDatabase(constants.PAGE_LENGTH);
    }
    // this.doGetChatsFromDatabaseThrottle();
  }

  render() {
    return (
      <div className="main-page">
        <div className="header">
          <SideMenu />
        </div>
        <div className="main-unit">
          <div className="top-bar">
            <Search onSearch={this.doGetChatsFromDatabase} />
          </div>
          <div className="chats">
            <Chats onScrollerEnd={this.doGetChatsFromDatabase} />
          </div>
        </div>
      </div>
    );
  }
}

const connectedMainPage = connect(
  state => ({
    chats: state.app.chats,
    token: state.auth.token,
    searchInput: state.app.searchInput,
  }),
  dispatch => ({
    exit(firebase) {
      dispatch(exit(firebase));
    },
    getChatsFromDatabase(database, limit, searchInput, status) {
      dispatch(updateChats(database, limit, searchInput, status));
    },
    setUserPic(pic) {
      dispatch(setNewProfilePic(pic));
    },
  })
)(MainPage);

export default connectedMainPage;
