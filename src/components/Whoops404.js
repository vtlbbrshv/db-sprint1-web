import React from "react";
import { withRouter } from "react-router-dom";

const Whoops404 = ({ history }) => (
  <div className="whoops-404">
    <h1>WRONG DOOR</h1>
    <button onClick={() => history.push("/")}>Main Page</button>
  </div>
);

export default withRouter(Whoops404);
