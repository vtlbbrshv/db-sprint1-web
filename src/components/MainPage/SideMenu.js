import React from "react";
import { connect } from "react-redux";
import { exit } from "../../actions/authActions";
import { withRouter, Link } from "react-router-dom";
import { routes } from "../../lib/constants";
import UserSettings from "../UserSettings/UserSettings";
import "../../stylesheets/SideMenu.scss";
import { getFirebase } from "react-redux-firebase";

//TODO при входе пользователя добавлять его данные в стейт (фото)

const SideMenu = ({ exitHandler = f => f, history, photo = "" }) => {
  const firebase = getFirebase();
  console.log(photo);
  return (
    <div className="side-menu">
      {photo ? (
        <img
          src={photo}
          alt="alt prop"
          style={{ maxHeight: "100px", maxWidth: "100px" }}
        />
      ) : (
        ""
      )}

      <ul>
        <li>
          <Link to="/">Активные</Link>
        </li>
        <li>
          <Link to={routes.NEW_CHATS}>Новые</Link>
        </li>
        <li>
          <Link to={routes.SAVED_CHATS}>Сохраненные</Link>
        </li>
        <li>
          <Link to={routes.COMPLETED_CHATS}>Завершенные</Link>
        </li>
      </ul>
      <ul>
        <li>
          <UserSettings />
        </li>
        <li>
          <button onClick={() => exitHandler(firebase)}>Выход</button>
        </li>
      </ul>
    </div>
  );
};

const connectedSideMenu = connect(
  state => ({ photo: state.profile.picUrl }),
  dispatch => ({
    exitHandler(firebase) {
      dispatch(exit(firebase));
    },
  })
)(SideMenu);

export default withRouter(connectedSideMenu);
