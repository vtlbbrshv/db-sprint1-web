import { actions } from "../lib/constants";

export const changeUserSettings = (user, email, password, picUrl) => ({
  type: actions.CHANGE_USER_SETTINGS,
  user: user,
  newEmail: email,
  newPassword: password,
  newPic: picUrl,
});

export const setNewProfilePic = picUrl => ({
  type: actions.SET_NEW_PROFILE_PIC,
  newPic: picUrl,
});

export const setGreeting = greeting => ({
  type: actions.SET_GREETING,
  greeting: greeting,
});

export const changeDefaultMessages = messages => ({
  type: actions.CHANGE_DEFAULT_MESSAGES,
  messages: messages,
});
