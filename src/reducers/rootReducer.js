import authReducer from "./authReducer";
import appReducer from "./appReducer";
import profileReducer from "./profileReducer";
import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { firebaseReducer } from "react-redux-firebase";

const authPersistConfig = {
  key: "auth",
  storage: storage,
  whitelist: ["token"],
  blacklist: ["err", "type"],
};

const appPersistConfig = {
  key: "app",
  storage: storage,
  blacklist: ["isSearching", "searchInput"],
};

const profilePersistConfig = {
  key: "profile",
  storage: storage,
  blacklist: ["picUrl"],
};

const rootReducer = combineReducers({
  form: formReducer,
  firebase: firebaseReducer,
  auth: persistReducer(authPersistConfig, authReducer),
  app: persistReducer(appPersistConfig, appReducer),
  profile: persistReducer(profilePersistConfig, profileReducer),
});

export default rootReducer;
