import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getFirebase } from "react-redux-firebase";
import ChatsScroller from "./ChatsScroller";

class ChatsPage extends Component {
  firebase = getFirebase();
  database = this.firebase.database();

  render() {
    return (
      <div className="chats-page">
        {this.props.chats ? (
          <ChatsScroller
            onScrollerEnd={this.props.onScrollerEnd}
            chats={this.props.chats}
          />
        ) : (
          "Загрузка...."
        )}
      </div>
    );
  }
}

const connectedChatsPage = connect(state => ({
  chats: state.app.chats,
}))(ChatsPage);

export default withRouter(connectedChatsPage);
