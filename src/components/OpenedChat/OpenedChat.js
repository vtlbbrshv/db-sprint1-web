import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { findById } from "../../lib/sortFunctions";
import Message from "../Chats/Message";
import Star from "../Chats/Star";
import MessageInput from "./MessageInput";

//диалоги не обновляются, тк не связаны с главной страницей

const OpenedChat = ({
  status = "",
  id,
  clientName = "",
  messages = [],
  rating,
  history,
}) => (
  <div className="opened-chat">
    <span> {clientName} </span>
    <button onClick={() => history.push("/")}>X</button>
    {status === "completed" ? (
      <p
        style={{ backgroundColor: "rgba(255, 0, 0, 0.5)", textAlign: "center" }}
      >
        Диалог завершен
      </p>
    ) : (
      ""
    )}
    {messages
      ? messages.map((message, i) => <Message key={i} message={message} />)
      : ""}
    <div>
      {rating
        ? [...Array(5)].map((n, i) => <Star key={i} selected={i < rating} />)
        : ""}
    </div>
    {status === "completed" ? (
      ""
    ) : (
      <MessageInput id={id} messageNumber={messages.length} />
    )}
  </div>
);

const connectedOpenedChat = connect((state, { match }) =>
  findById(state.app.chats, Number(match.params.id))
)(OpenedChat);

export default withRouter(connectedOpenedChat);
