import "@testing-library/jest-dom/extend-expect";
import React from "react";
import Enzyme, { shallow } from "enzyme";
import { LoginForm } from "../../../components/LoginForm/LoginForm";

import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });
console.error = message => {
  throw new Error(message);
};

describe("Тесты", () => {
  it("render <LoginForm/>", () => {
    const Component = shallow(<LoginForm />);
    expect(Component.find("form")).toHaveLength(1);
  });

  it("render buttons", () => {
    jest.setTimeout(30000);
    const Component = shallow(<LoginForm />);
    expect(Component.find("button")).toHaveLength(2);
  });

  test("render children", () => {
    const Component = shallow(<LoginForm />);
    expect(Component.find('[name="email"]')).toHaveLength(1);
    expect(Component.find('[name="password"]')).toHaveLength(1);
    expect(Component.find("button")).toHaveLength(2);
  });
});
