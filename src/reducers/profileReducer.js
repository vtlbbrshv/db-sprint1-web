import { actions } from "../lib/constants";

const profile = (state = {}, action = { type: null }) => {
  switch (action.type) {
    case actions.SET_NEW_PROFILE_PIC: {
      return { ...state, picUrl: action.newPic };
    }

    case actions.SET_GREETING: {
      return { ...state, autogreeting: action.greeting };
    }

    case actions.CHANGE_DEFAULT_MESSAGES: {
      return {
        ...state,
        defaultMessages: action.messages,
      };
    }

    default:
      return state;
  }
};

export default profile;
