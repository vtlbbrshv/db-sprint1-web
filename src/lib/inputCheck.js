export const required = value =>
  value ? undefined : "Поле не должно быть пустым";

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Некорректно введен email"
    : undefined;
