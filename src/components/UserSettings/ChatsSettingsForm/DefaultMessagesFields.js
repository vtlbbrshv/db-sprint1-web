import React, { Component } from "react";
import { connect } from "react-redux";
import { Field } from "redux-form";
import { inputField } from "../../Fields";
import { changeDefaultMessages } from "../../../actions/profileActions";

class defaultMessagesFields extends Component {
  constructor(props) {
    super(props);
    this.state = { defaultMessages: this.props.fields };
  }

  //TODO синхронизировать готовые сообщения с фаербейсом (то есть хранить их в настройках юзера)
  //TODO задать вопрос: нужно ли вообще хранить это все в настройках юзера в фаербейсе?
  // или достаточно просто в стейте приложения
  // преимущества: одни настройки для всех аккаунтов человека

  saveHandle = e => {
    e.preventDefault();
    this.props.saveDefaultMessages(this.state.defaultMessages);
  };

  render() {
    const {
      meta: { error, submitFailed },
    } = this.props;

    return (
      <div>
        <ul>
          <li>
            <button
              type="button"
              onClick={() =>
                this.setState({
                  defaultMessages: [...this.state.defaultMessages, "Hi"],
                })
              }
            >
              Добавить
            </button>
            {submitFailed && error && <span>{error}</span>}
          </li>
          {this.state.defaultMessages.map((message, i) => (
            <li key={i}>
              <Field
                name={"defaultMessage_" + i}
                type="text"
                component={inputField}
                label={message}
                onChange={e =>
                  this.setState({
                    defaultMessages: this.state.defaultMessages.map(
                      (message, j) => (j === i ? e.target.value : message)
                    ),
                  })
                }
              />
              <button
                type="button"
                title="Убрать"
                onClick={() =>
                  this.setState({
                    defaultMessages: this.state.defaultMessages.filter(
                      (m, j) => i !== j
                    ),
                  })
                }
              >
                Убрать
              </button>
            </li>
          ))}
        </ul>
        <button onClick={this.saveHandle}>Сохранить</button>
      </div>
    );
  }
}

const connectedDefaultMessagesFields = connect(null, dispatch => ({
  saveDefaultMessages(messages) {
    dispatch(changeDefaultMessages(messages));
  },
}))(defaultMessagesFields);

export default connectedDefaultMessagesFields;
