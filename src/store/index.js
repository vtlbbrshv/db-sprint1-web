import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import reducer from "../reducers/rootReducer";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import initialState from "./initialState";

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["auth", "app", "profile"],
};

const persistedReducer = persistReducer(persistConfig, reducer);

const configureStore = () => {
  let store = {
    ...createStore(
      persistedReducer,
      initialState,
      applyMiddleware(sagaMiddleware)
    ),
    runSaga: sagaMiddleware.run,
  };

  let persistor = persistStore(store);
  return { store, persistor };
};

export default configureStore;
