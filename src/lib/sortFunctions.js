export const sortByStatus = (chats, status) =>
  chats.filter(chat => (chat ? chat.status === status : false));

export const search = (chats, searchInput) => {
  const nameFiltered = chats.filter(chat =>
    chat.clientName.toLowerCase().includes(searchInput.toLowerCase())
  );
  const messageFiltered = chats.filter(
    chat =>
      chat.messages.filter(message =>
        message.content.toLowerCase().includes(searchInput.toLowerCase())
      ).length !== 0
  );
  const res = [...nameFiltered, ...messageFiltered];
  return res.filter((chat, pos) => res.indexOf(chat) === pos);
};

export const findById = (array, id) => array.filter(item => item.id === id)[0];
