import React from "react";
import ReactDOM from "react-dom";
import "./stylesheets/index.scss";
import App from "./components/App";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import configureStore from "./store/index";
import rootSaga from "./sagas/index";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter as Router } from "react-router-dom";

import { ReactReduxFirebaseProvider } from "react-redux-firebase";
import { rrfConfig, fbConfig } from "./lib/config";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const { store, persistor } = configureStore();
firebase.initializeApp(fbConfig);

store.runSaga(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ReactReduxFirebaseProvider
        firebase={firebase}
        config={rrfConfig}
        dispatch={store.dispatch}
      >
        <Router>
          <App store={store} />
        </Router>
      </ReactReduxFirebaseProvider>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
