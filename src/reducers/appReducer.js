import { actions } from "../lib/constants";

const chat = (state = {}, action = { type: null }) => {
  switch (action.type) {
    case actions.ADD_CHATS_TO_STATE: {
      return { ...state, chats: action.data };
    }

    case actions.SEARCH: {
      return { ...state, searchInput: action.input };
    }

    default:
      return state;
  }
};

export default chat;
