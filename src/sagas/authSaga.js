import { call, put, takeLatest } from "redux-saga/effects";
import { actions } from "../lib/constants";
import { reqFailed, reqSuccess, deleteToken } from "../actions/authActions";

function* loginSaga(action) {
  const res = yield call(() =>
    action.firebase
      .login({ email: action.email, password: action.password })
      .then(res => res.user)
      .catch(err => err)
  );
  res.user
    ? yield put(reqSuccess(actions.LOGIN_SUCCESS, res.user.uid))
    : yield put(reqFailed(actions.LOGIN_FAILURE, res.message));
}

function* registrationSaga(action) {
  const res = yield call(() =>
    action.firebase
      .auth()
      .createUserWithEmailAndPassword(action.email, action.password)
      .then(res => res)
      .catch(error => error)
  );
  res.user
    ? yield put(reqSuccess(actions.REGISTRATION_SUCCESS))
    : yield put(reqFailed(actions.REGISTRATION_FAILURE, res.message));
}

function* exitSaga(action) {
  const err = yield call(() =>
    action.firebase
      .auth()
      .signOut()
      .then(res => res)
      .catch(err => err)
  );
  if (!err) {
    yield put(deleteToken());
  }
}

export function* watchAuthSaga() {
  yield takeLatest(actions.LOGIN_REQUEST, loginSaga);
  yield takeLatest(actions.REGISTRATION_REQUEST, registrationSaga);
  yield takeLatest(actions.EXIT, exitSaga);
}
