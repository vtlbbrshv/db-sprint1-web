import { call, put, takeLatest } from "redux-saga/effects";
import { actions } from "../lib/constants";
import { setNewProfilePic } from "../actions/profileActions";

//Important: To set a user's email address, the user must have signed in recently. See Re-authenticate a user.
function* changeUserSettingsSaga(action) {
  if (action.newEmail !== action.user.email) {
    yield call(() =>
      action.user.updateEmail(action.newEmail).catch(err => console.log(err))
    );
  }

  if (action.newPassword) {
    yield call(() =>
      action.user
        .updatePassword(action.newPassword)
        .catch(err => console.log(err))
    );
  }

  if (action.newPic !== action.user.photoURL) {
    yield call(() => action.user.updateProfile({ photoURL: action.newPic }));
    yield put(setNewProfilePic(action.newPic));
  }
}

export function* watchProfileSaga() {
  yield takeLatest(actions.CHANGE_USER_SETTINGS, changeUserSettingsSaga);
}
