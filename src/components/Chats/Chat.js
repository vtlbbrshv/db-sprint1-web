import React from "react";
import Message from "./Message";
import { getFirebase } from "react-redux-firebase";
import Star from "./Star";
import { connect } from "react-redux";
import { changeStatus, sendMessage } from "../../actions/appActions";
import { withRouter } from "react-router-dom";
import { constants, status } from "../../lib/constants";

const Chat = ({
  chat = {},
  changeStatusOnServer = f => f,
  history,
  greeting = f => f,
  autogreeting,
}) => {
  const firebase = getFirebase();
  const database = firebase.database();

  const enterChat = id => {
    changeStatusOnServer(
      database,
      id,
      "active",
      constants.PAGE_LENGTH,
      status.NEW
    );

    greeting(database, chat.id, chat.messages.length, autogreeting);
  };

  return (
    <div className="chat">
      <p className="client-name">{chat.clientName}:</p>
      {chat.messages
        ? chat.messages.map((message, i) => (
            <Message key={i} message={message} />
          ))
        : ""}
      {chat.status !== "new" ? (
        <button onClick={() => history.push("/" + chat.id)}>Открыть</button>
      ) : (
        ""
      )}

      {chat.status === "new" ? (
        <button onClick={() => enterChat(chat.id)}>Войти в диалог</button>
      ) : (
        ""
      )}
      {chat.status === "saved" ? (
        <button
          onClick={() =>
            changeStatusOnServer(
              database,
              chat.id,
              "active",
              constants.PAGE_LENGTH,
              status.SAVED
            )
          }
        >
          Удалить
        </button>
      ) : (
        ""
      )}
      {chat.status === "active" ? (
        <button
          onClick={() =>
            changeStatusOnServer(
              database,
              chat.id,
              "saved",
              constants.PAGE_LENGTH,
              status.ACTIVE
            )
          }
        >
          Сохранить
        </button>
      ) : (
        ""
      )}
      {chat.rating
        ? [...Array(5)].map((n, i) => (
            <Star key={i} selected={i < chat.rating} />
          ))
        : ""}
    </div>
  );
};

const connectedChat = connect(
  state => ({
    autogreeting: state.profile.autogreeting,
  }),
  dispatch => ({
    changeStatusOnServer(database, id, newStatus, limit, status) {
      dispatch(changeStatus(database, id, newStatus, limit, status));
    },
    greeting(database, chatId, messageId, message) {
      dispatch(sendMessage(database, chatId, messageId, message));
    },
  })
)(Chat);

export default withRouter(connectedChat);
